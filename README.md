# Dice Roller

A dice rolling app for Ubuntu Touch. Contains various different kinds of dice
for use with many different kinds of games and pen & paper RPGs!

This a fork of the [Dice Roller app by Robert Ancell](https://launchpad.net/dice-roller).

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/dice-roller.bhdouglass)

Now with added color. If you precede your first value on a custom die with a standard color name in square brackets, the text or dots will be in that color.
eg. Name: RedDie, SideValues: [red]., ., ., ., ., .
eg. Name: HappySad_coin, SideValues: U+263A, U+2639

## Contributions

A huge thank you to Joan CiberSheep for his icons for the app and dice
(based on icons from [the noun project](http://thenounproject.com/)).

Color and Unicode entry added by JassMan23

## Development

Build and run using [clickable](https://github.com/bhdouglass/clickable).

## Translations

Translations are handled online via [Weblate](https://translate-ut.org/projects/dice-roller/dice-roller/).
A huge thank you to all the translators!

## Donate

If you like Dice Roller, consider giving a small donation over at my
[Liberapay page](https://liberapay.com/bhdouglass).

## License

Copyright (C) 2019 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
